from random import randint

name = input("Hi! What is your name? ")

month_list = ["January", "Feburary", "March",
    "April", "May", "June", "July", "August", "September", "November", "December"]

guess_number = 0

for num in range(5):
    guess_number += 1
    month_number = randint(0,11)
    year_number = randint(1924,2004)

    month_name = month_list[month_number]

    print("Guess ", guess_number, ": ", name, "were you born in",
    month_name, year_number, "?")

    response = input("yes or no? ")
    if guess_number == 5 and response == "no":
        print("I have other things to do. Good bye!!!")
        exit()
    if response == "yes":
        print("I knew it!")
        exit()
    if guess_number < 5 and response == "no":
        print("Drat! Let me try again!")
